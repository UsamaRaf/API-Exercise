CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS titanic (
    uuid uuid NOT NULL DEFAULT uuid_generate_v4(),
    survived boolean NOT NULL,
    pclass numeric NOT NULL,
    p_name character varying COLLATE pg_catalog."default" NOT NULL,
    sex character varying COLLATE pg_catalog."default" NOT NULL,
    age numeric NOT NULL,
    siblings_spouse_aboard numeric NOT NULL,
    parents_children_aboard numeric NOT NULL,
    fare numeric NOT NULL,
    CONSTRAINT titanic_pkey PRIMARY KEY (uuid)
);

ALTER TABLE titanic
    OWNER to postgres;


COPY titanic(survived, pclass, p_name, sex,age,siblings_spouse_aboard,parents_children_aboard,fare)
FROM '/var/lib/postgresql/csvs/titanic.csv'
DELIMITER ','
CSV HEADER;
