/// <summary>
/// Entry point of API.
/// </summary>

// Get all required modules and define constants.
const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const db = require("./queries");
const inputValidator = require("./validator");
const port = process.env.PORT || 3000;

// Use JSON body parser.
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

// API endpoint for getting all poeple from the Database. GET '/people'
app.get("/people", db.getPerson);

// API endpoint for getting a person with specific uuid from the Database. GET '/people/:uuid"'
app.get(
  "/people/:uuid",
  inputValidator.validate("getPersonById"),
  db.getPersonById
);

// API endpoint to add a new person data to the Database. POST '/people'
app.post("/people", inputValidator.validate("addPerson"), db.addPerson);

// API endpoint to update a person data with specific uuid in the Database. PUT '/people/:uuid"'
app.put(
  "/people/:uuid",
  inputValidator.validate("updatePersonData"),
  db.updatePersonData
);

// API endpoint to delate a person data with specific uuid from the Database. DELETE '/people/:uuid"'
app.delete(
  "/people/:uuid",
  inputValidator.validate("deletePerson"),
  db.deletePerson
);

// API is accessible on port 3000 by default.
app.listen(port, () => {
  console.log(`App running on port ${port}.`);
});
