/// <summary>
/// Contains functions with actual implementation for API end points.
/// </summary>

// Get Connection to Postgres DB.
const Pool = require("pg").Pool;
const pool = new Pool({
  user: "postgres",
  host: "postgres",
  database: "csolutions",
  password: "newPassword", // Should be secured in production env.
  port: 5432,
});

const { validationResult } = require("express-validator");

// Implementation to get all people from the DB.
const getPerson = (request, response) => {
  pool.query("SELECT * FROM titanic", (error, results) => {
    if (error) {
      throw error;
    }
    response.status(200).json(results.rows);
  });
};

// Implementation to get all a person bu uuid from the DB.
const getPersonById = (request, response, next) => {
  try {
    const errors = validationResult(request); // Finds the validation errors in this request.

    // Check if we found any validation errors.
    if (!errors.isEmpty()) {
      // Errors found, report and return.
      response.status(422).json({ errors: errors.array() });
      return;
    }

    // No Validation errors found. Execute query and get response. 
    const uuid = String(request.params.uuid);
    pool.query(
      "SELECT * FROM titanic WHERE uuid = $1",
      [uuid],
      (error, results) => {
        if (error) {
          throw error;
        }
        response.status(200).json(results.rows);
      }
    );
  } // end try.
  catch (err) {
    return next(err);
  }
};

// Implementation to add a new person to the DB.
const addPerson = (request, response, next) => {
  try {
    const errors = validationResult(request); // Finds the validation errors in this request.

    // Check if we found any validation errors.
    if (!errors.isEmpty()) {
      // Errors found, report and return.
      response.status(422).json({ errors: errors.array() });
      return;
    }

    // No Validation errors found. Execute query and get response.
    const {
      survived,
      pclass,
      p_name,
      sex,
      age,
      siblings_spouse_aboard,
      parents_children_aboard,
      fare,
    } = request.body;

    // Execute actual query.
    pool.query(
      "INSERT INTO titanic (survived, pclass, p_name, sex, age, siblings_spouse_aboard, parents_children_aboard, fare)" +
          "VALUES ($1, $2, $3, $4, $5, $6, $7, $8)",
      [
        survived,
        pclass,
        p_name,
        sex,
        age,
        siblings_spouse_aboard,
        parents_children_aboard,
        fare,
      ],
      (error, results) => {
        if (error) {
          throw error;
        }
        response.status(201).send("Person Added");
      }
    );
  } // End try.
  catch (err) {
    return next(err);
  }
};

// Implementation to update a person data in the DB.
const updatePersonData = (request, response, next) => {
  try {
    const errors = validationResult(request); // Finds the validation errors in this request.
    
    // Check if we found any validation errors.
    if (!errors.isEmpty()) {
      // Errors found, report and return.
      response.status(422).json({ errors: errors.array() });
      return;
    }

    // No Validation errors found. Execute query and get response.
    const uuid = String(request.params.uuid);
    const {
      survived,
      pclass,
      p_name,
      sex,
      age,
      siblings_spouse_aboard,
      parents_children_aboard,
      fare,
    } = request.body;

    // Execute actual query.
    pool.query(
      "UPDATE titanic SET survived = $1, pclass = $2, p_name = $3, sex = $4, age = $5," +
          "siblings_spouse_aboard = $6, parents_children_aboard = $7, fare = $8 WHERE uuid = $9",
      [
        survived,
        pclass,
        p_name,
        sex,
        age,
        siblings_spouse_aboard,
        parents_children_aboard,
        fare,
        uuid,
      ],
      (error, results) => {
        if (error) {
          throw error;
        }

        // Check if the any person data was updated.
        if (results.rowCount == 0)
          response.status(200).send(`Person does not exist with ID: ${uuid}`);

        // No data updated.
        else 
          response.status(200).send(`Person data updated with ID: ${uuid}`);
      }
    );
  } // End try.
  catch (err) {
    return next(err);
  }
};

// Implementation to delate a person data from the DB.
const deletePerson = (request, response, next) => {
  try {
    const errors = validationResult(request); // Finds the validation errors in this request.

    // Check if we found any validation errors.
    if (!errors.isEmpty()) {
      // Errors found, report and return.
      response.status(422).json({ errors: errors.array() });
      return;
    }

    // No Validation errors found. Execute query and get response.
    const uuid = String(request.params.uuid);

    pool.query(
      "DELETE FROM titanic WHERE uuid = $1",
      [uuid],
      (error, results) => {
        if (error) {
          throw error;
        }

        // Check if the any person data was deleted.
        if (results.rowCount == 0)
          response.status(200).send(`Person does not exist with ID: ${uuid}`);

        // No data deleted.
        else 
          response.status(200).send(`Person deleted with ID: ${uuid}`);
      }
    );
  } // End try.
  catch (err) {
    return next(err);
  }
};

// Export all functions to be used in other modules.
module.exports = {
  getPerson,
  getPersonById,
  addPerson,
  updatePersonData,
  deletePerson,
};
