/// <summary>
/// Validator for the API parameters and payload.
/// </summary>

// Get the express validator module.
const { body, param } = require("express-validator");

// Actual validation.
const validate = (method) => {
  switch (method) {
    case "addPerson": { // Validator to check body parameters of addPerson API call.
      return [
        body(
          "survived",
          "Parameter should not be empty and should be a Boolean value"
        ).exists().isBoolean(), // 'survived' parameter should exist and it should be a boolean value.
        body("pclass", "Invalid Passenger Class").exists().isNumeric().isIn([1, 2, 3]), //'pclass' parameter should exist, should be numeric and it should have value 1,2, or 3.
        body("p_name").isString().exists(), // 'p_name' parameter should exist and it should be a string value.
        body("sex").isString().exists(), // 'sex' parameter should exist and it should be a string value.
        body("siblings_spouse_aboard").isNumeric().exists(), // 'siblings_spouse_aboard' parameter should exist and it should be a numeric value.
        body("parents_children_aboard").isNumeric().exists(), // 'parents_children_aboard' parameter should exist and it should be a numeric value.
        body("fare").isNumeric().exists(), // 'fare' parameter should exist and it should be a numeric value.
      ];
    }
    case "getPersonById":
    case "deletePerson": { // Validator to check parametrs of getPersonById and deletePerson API call.
      return [param("uuid").exists().isUUID()]; // UUID should be in correct format.
    }
    case "updatePersonData": { // Validator to check parametrs of updatePersonData API call.
      return [
        param("uuid").exists().isUUID(), // UUID should be in correct format.
        body(
          "survived",
          "Parameter should not be empty and should be a Boolean value"
        ).exists().isBoolean(), // 'survived' parameter should exist and it should be a boolean value.
        body("pclass", "Invalid Passenger Class").exists().isNumeric().isIn([1, 2, 3]), //'pclass' parameter should exist, should be numeric and it should have value 1,2, or 3.
        body("p_name").isString().exists(), // 'p_name' parameter should exist and it should be a string value.
        body("sex").isString().exists(), // 'sex' parameter should exist and it should be a string value.
        body("siblings_spouse_aboard").isNumeric().exists(), // 'siblings_spouse_aboard' parameter should exist and it should be a numeric value.
        body("parents_children_aboard").isNumeric().exists(), // 'parents_children_aboard' parameter should exist and it should be a numeric value.
        body("fare").isNumeric().exists(), // 'fare' parameter should exist and it should be a numeric value.
      ];
    }
  }
};

// Export the module to be used in other modules.
module.exports = {
  validate,
};
