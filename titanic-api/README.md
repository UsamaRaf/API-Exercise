# API-exercise

This exercise has been implemented with following technologies:
* **node.js** is used for API development. node.js is lightweight, fast and scalable. It is very effecient for API development.
* **PostgreSQL** is used for Database. It is an open-source, highly extensible database.
* **docker-compose** is used for building and running Docker containers for both API and Database.
* **kubectl** was used for testing the Kubernetes deployment on local Kubernetes cluster availabe on Docker Desktop.

## Project Structure

All the code and scripts are under the folder **titanic-api**.

#### Dockerfile and docker-compose files
In the root of this folder, there are docker-compose and Dockerfile. Dockerfile is for creating an image for our API server. docker-compose allows building and deploying of both API and Database containers with a single command.

#### src directory
All the API realted node.js code and migrations needed for DB are  under the src/ folder. It contains the main entry point to the API in **index.js** file, **queries.js** is for connecting to DB and executing all DB realted operations for API, and **validator.js** is for validating the API requests, sanitizing them and validating the payloads.
**src/migrations/** folder contains the script for DB schema initialization and a .csv file for populating Table with initial data.

#### deployments directory
It contains .yaml files for creating all the modules e.g. Pods, Services, Volumes, Secrets etc for Kubernetes deployment. It also contains a **deploy-k8.ps1** script which deploys everything to Kubernetes cluster with one command.

## Instructions to Run the API
### 1. Run Docker Containers on local machine

In the folder **titanic-api**, there is docker-compose.yml file which can be used to run API and Postgres DB containers. You need to have a Docker Engine running on your machine. E.g Docker Desktop on Mac or Windows.
You can use cmd to change your directory to titanic-api/ and Run the following command to build and run containers.
* docker-compose up

Once the containers are up and running, you can access the API at **localhost:3005**. E.g. Access localhost:3005/people from you browser and get all the people from the Database. You can use e.g. Postman to test all the API endpoints accordig to the [specification](../API.md).

#### Implementation Details
* Using the Dockerfile in the titanic-api directory, the image **titanic-node-server** is created and used to build and run the contianer for API. Container port 3005 is exposed and mapped to local port 3005, this is the port where API is accessible.
* postgres container for DB is using volumes for persistent storage. This makes sure that our data is not lost when container is recreated.

### 2. Deploy and Run Containers on local Kubernetes Cluster
You can test the Kubernetes deployment on a local Kubernetes Cluster. Docker Desktop provides native support for a Kubernetes Cluster. You check the details [here](https://docs.docker.com/docker-for-windows/#kubernetes)  on how to enable it. This will also install kubectl on your machine which is required for deployment. Once you have the cluster running, change directory to titanic-api/deployments/ and run following powershell script.

* ./deploy-k8.ps1

Once the containers are deployed and running as pods and services, you can access the API at **localhost:3000**. E.g. Access localhost:3000/people from you browser and get all the people from the Database. You can use e.g. Postman to test all the API endpoints accordig to the [specification](../API.md)

#### Implementation Details
* Secrets are created and used to access the password for connecting to the Database. The password is base64 encoded.
* Images for node [API](https://hub.docker.com/r/rafique01/titanic-node-server) and [postgres](https://hub.docker.com/r/rafique01/titanic-db) are uploaded to the public Docker Hub repository to be used and accessed for Kubernetes deployment.
* Persistent volumes are used for postgres deployment.


